
(function () {
    "use strict";
  
    let forms = document.querySelectorAll('.mail-form');
  
    forms.forEach( function(e) {
      e.addEventListener('submit', function(event) {
        event.preventDefault();
  
        let thisForm = this;
  
        let action = thisForm.getAttribute('action');
        
        if( ! action ) {
          displayError(thisForm, 'The form action property is not set!')
          return;
        }

        thisForm.querySelector('.cargando').classList.remove('d-none');
        thisForm.querySelector('.mensaje-error').classList.add('d-none');
        thisForm.querySelector('.mensaje-enviado').classList.add('d-none');
  
        let formData = new FormData( thisForm );
  
        
        php_email_form_submit(thisForm, action, formData);
     
      });
    });
  
    function php_email_form_submit(thisForm, action, formData) {
      fetch(action, {
        method: 'POST',
        body: formData,
        headers: {'X-Requested-With': 'XMLHttpRequest'}
      })
      .then(response => {
        if( response.ok ) {
          return response.text()
        } else {
          throw new Error(`${response.status} ${response.statusText} ${response.url}`); 
        }
      })
      .then(data => {

        thisForm.querySelector('.cargando').classList.add('d-none');
        if (data.trim() == 'OK') {
          thisForm.querySelector('.mensaje-enviado').classList.remove('d-none');
          thisForm.reset(); 
        } else {
          throw new Error(data ? data : 'Form submission failed and no error message returned from: ' + action); 
        }
      })
      .catch((error) => {
        displayError(thisForm, error);
      });
    }
  
    function displayError(thisForm, error) {
      thisForm.querySelector('.cargando').classList.add('d-none');
      thisForm.querySelector('.mensaje-error').innerHTML = error;
      thisForm.querySelector('.mensaje-error').classList.remove('d-none');
    }
  
  })();
  